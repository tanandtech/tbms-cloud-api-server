const {
  InfluxDB,
  fluxString,
  flux,
  fluxExpression,
  fluxInteger,
} = require('@influxdata/influxdb-client');
const config = require('config');

const { influx } = config;

const { url, token, org } = influx;

const influxDB = new InfluxDB({ url, token });

const queryApi = influxDB.getQueryApi(org);
const influxModule = {};

influxModule.flux = (
  project,
  site,
  measurement,
  start,
  end,
  interval,
  deviceId,
  fields,
  agg = 'mean',
) => {
  let fieldsFilter = '';

  if (fields && fields.trim().length > 0) {
    fieldsFilter = `|> filter(fn: (r) => ${fields
      .trim()
      .split(',')
      .map((r) => `r._field == ${fluxString(r)}`)
      .join(' or ')})`;
  }

  const query = `from(bucket: "${project}")
                        |> range(start: ${parseInt(start, 10)}, stop: ${parseInt(end, 10)})
                        |> filter(fn: (r) => r["_measurement"] == "${measurement}")
                        |> filter(fn: (r) => r["deviceId"] == "${deviceId}")
                        |> filter(fn: (r) => r["site"] == "${site}")
                        ${fluxExpression(fieldsFilter)}
                        |> aggregateWindow(
                            every: ${parseInt(interval, 10)}s, 
                            fn: ${fluxExpression(agg)}, 
                            createEmpty: false)
                        |> yield()`;

  return queryApi.collectRows(query);
};

influxModule.getFieldKeys = (project, site, deviceId) => {
  const query = flux`import "influxdata/influxdb/schema"
    schema.fieldKeys(
      bucket: "${project}",
      predicate: (r) => r.site == ${fluxString(site)} 
        and r.deviceId == ${fluxString(deviceId)}
    )`;
  return queryApi.collectRows(query);
};

influxModule.getGraphData = (project, site, deviceId, field, start, end, interval, agg) => {
  const query = flux`
    from(bucket: "${project}")
      |> range(start: ${fluxInteger(start)}, stop: ${fluxInteger(end)})
      |> filter(fn: (r) => r.site == ${fluxString(site)} 
          and r.deviceId == ${fluxString(deviceId)}
          and r._field == ${fluxString(field)} )
      |> aggregateWindow(every: ${fluxInteger(interval)}s, fn: ${fluxExpression(
    agg,
  )}, createEmpty: false)
      |> yield()`;
  return queryApi.collectRows(query);
};

influxModule.getLatestData = (project, site, measurement, deviceId, fields) => {
  const fieldsFilter =
    fields && fields.trim().length > 0
      ? `|> filter(fn: (r) => ${fields
          .trim()
          .split(',')
          .map((r) => `r._field == ${fluxString(r)}`)
          .join(' or ')})`
      : '';
  const fluxQuery = flux`from(bucket: "${project}") 
    |> range(start: -15m)
    |> filter(fn: (r) => r.site == "${site}")
    ${fluxExpression(fieldsFilter)}
    |> filter(fn: (r) => r.deviceId == "${deviceId}")
    |> filter(fn: (r) => r._measurement == "${measurement}")
    |> group(columns: ["_field"])
    |> last()`;
  return queryApi.collectRows(fluxQuery);
};

module.exports = influxModule;
