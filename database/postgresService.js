const { Pool } = require('pg');
const moment = require('moment');
const config = require('config');

const { postgres } = config;

const { user, host, database, password, port } = postgres;

const pgSvc = new Pool({
  user,
  host,
  database,
  password,
  port,
});

// pgSvc.connect();

/** ****************
 *
 * ALERTS API
 *
 ***************** */
exports.getAllAlerts = (project, site) => {
  const query = `select * from alerts where project = '${project}' and site = '${site}';`;
  return pgSvc.query(query);
};

exports.insertAlert = (project, site, deviceId, level, type, val, timestamp) => {
  const query = `
    insert into alerts (project, site, deviceId, level, type, value, timestamp) 
    values (
      '${project}', '${site}', '${deviceId}', '${level}', '${type}', '${val}', 
      to_timestamp(${moment(timestamp / 1000000).unix()})
    );
  `;

  return pgSvc.query(query);
};

exports.getBenchmark = async (project, site, deviceId) => {
  const query = `SELECT * from benchmark WHERE project_id = $1 AND site_id = $2 AND device_id = $3`;
  const result = await pgSvc.query(query, [project, site, deviceId]);

  return result.rows[0];
};

exports.deleteBenchmarkData = (benchmarkId) => {
  const query = `DELETE FROM benchmark_data where benchmark_id = $1`;
  return pgSvc.query(query, [benchmarkId]);
};

exports.createBenchmark = async (project, site, deviceId, start, end, keys) => {
  const query = `INSERT INTO benchmark(project_id, site_id, device_id, start_timestamp, end_timestamp, created_timestamp, keys) 
    VALUES($1, $2, $3, $4, $5, $6, $7) 
    ON CONFLICT ON CONSTRAINT benchmark_un  
      DO UPDATE SET start_timestamp = EXCLUDED.start_timestamp, end_timestamp = EXCLUDED.end_timestamp, created_timestamp = EXCLUDED.created_timestamp, keys = EXCLUDED.keys
    RETURNING id`;
  const result = await pgSvc.query(query, [
    project,
    site,
    deviceId,
    start,
    end,
    Math.round(Date.now() / 1000),
    keys,
  ]);

  return result.rows[0];
};

exports.createBenchmarkData = (points) => {
  const query = `INSERT INTO public.benchmark_data
  (benchmark_id, "key", "timestamp", value, timestamp_10m, timestamp_30m, timestamp_1h, timestamp_1d, group_5m_day, group_10m_day, group_30m_day, group_hour_day, group_dom, group_dow)
  SELECT * FROM UNNEST ($1::int[], $2::text[], $3::int8[], $4::float4[], $5::int8[], $6::int8[], $7::int8[], $8::int8[], $9::int4[], $10::int4[], $11::int4[], $12::int4[], $13::int2[], $14::int2[])`;
  const benchmarkIds = [];
  const keys = [];
  const timestamps = [];
  const values = [];
  const timestamp10ms = [];
  const timestamp30ms = [];
  const timestamp1hs = [];
  const timestamp1ds = [];
  const group5ms = [];
  const group10ms = [];
  const group30ms = [];
  const group1hs = [];
  const groupdoms = [];
  const groupdows = [];

  points.forEach((p) => {
    benchmarkIds.push(p.benchmarkId);
    keys.push(p.key);
    timestamps.push(p.timestamp);
    values.push(p.value);
    timestamp10ms.push(p.timestamp_10m);
    timestamp30ms.push(p.timestamp_30m);
    timestamp1hs.push(p.timestamp_1h);
    timestamp1ds.push(p.timestamp_1d);
    group5ms.push(p.group_5m);
    group10ms.push(p.group_10m);
    group30ms.push(p.group_30m);
    group1hs.push(p.group_1h);
    groupdoms.push(p.group_dom);
    groupdows.push(p.group_dow);
  });

  return pgSvc.query(query, [
    benchmarkIds,
    keys,
    timestamps,
    values,
    timestamp10ms,
    timestamp30ms,
    timestamp1hs,
    timestamp1ds,
    group5ms,
    group10ms,
    group30ms,
    group1hs,
    groupdoms,
    groupdows,
  ]);
};

const getAggFn = (agg) => {
  if (agg === 'sum') {
    return 'SUM';
  }
  return 'AVG';
};

exports.getBenchmarkAgg = (benchmarkId, agg, dow, groupKey, keys, groupAgg) => {
  const aggFn = getAggFn(agg);
  const groupAggFn = getAggFn(groupAgg);

  const query = `SELECT key, ${aggFn}(value) AS value FROM (
      SELECT key, ${groupKey} as timestamp, ${groupAggFn}(value) AS value FROM benchmark_data  
      WHERE benchmark_id = $1 and key = ANY($2) ${dow ? 'AND group_dow = $3' : ''}
      GROUP BY key, ${groupKey}
    ) t
    GROUP BY key
    ORDER BY key`;
  const params = [benchmarkId, keys];
  if (dow) params.push(dow);
  return pgSvc.query(query, params);
};

exports.getBenchmarkAggData = (benchmarkId, agg, dow, groupKey, keys) => {
  let aggFn = 'AVG';
  if (agg === 'sum') {
    aggFn = 'SUM';
  }

  const query = `SELECT key, ${groupKey} as timestamp, ${aggFn}(value) AS value FROM benchmark_data  
    WHERE benchmark_id = $1 AND key = ANY($2) ${dow ? 'AND group_dow = $3' : ''}
    GROUP BY key, ${groupKey} 
    ORDER BY key, ${groupKey}`;

  const params = [benchmarkId, keys];
  if (dow) params.push(dow);

  return pgSvc.query(query, params);
};

exports.getSites = (project) => {
  const query = `SELECT * FROM site WHERE project_id = $1`;
  return pgSvc.query(query, [project]);
};

exports.getSite = (site) => {
  const query = `SELECT * FROM site WHERE id = $1`;
  return pgSvc.query(query, [site]);
};

exports.getAreas = (site) => {
  const query = 'SELECt * FROM area WHERE site_id = $1';
  return pgSvc.query(query, [site]);
};

exports.getEquipments = (site) => {
  const query = 'SELECT * FROM equipment WHERE site_id = $1';
  return pgSvc.query(query, [site]);
};

exports.getEquipmentsByArea = (area) => {
  const query = `SELECT e.* FROM equipment e, equipment_area ea 
    WHERE e.id = ea.equipment_id AND ea.area_id = $1`;

  return pgSvc.query(query, [area]);
};

exports.getAreaByEquipment = (equipment) => {
  const query = `SELECT a.* FROM area a, equipment_area ea 
    WHERE a.id = ea.area_id AND ea.equipment_id = $1`;

  return pgSvc.query(query, [equipment]);
};

exports.getDevices = (site) => {
  const query = `SELECT * FROM device
    WHERE site_id = $1`;

  return pgSvc.query(query, [site]);
};
