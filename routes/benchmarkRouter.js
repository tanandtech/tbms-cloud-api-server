const express = require('express');
const middleware = require('../middlewares/userMiddleware');

const router = express.Router();

const benchmarkController = require('../controllers/benchmarkController');

// get benchmark detail
router.get('/:project/:site/:deviceId', middleware.validate, benchmarkController.getBenchmark);

// get aggregated benchmark
router.get(
  '/:project/:site/:deviceId/:agg',
  middleware.validate,
  benchmarkController.getBenchmarkAgg,
);

// get benchmark data
router.get(
  '/:project/:site/:deviceId/:interval/:agg',
  middleware.validate,
  benchmarkController.getBenchmarkAggData,
);

// set benchmark
router.post('/:project/:site/:deviceId', middleware.validate, benchmarkController.setBenchmark);

module.exports = router;
