const express = require('express');

const router = express.Router();

const middleware = require('../middlewares/userMiddleware');
const generalController = require('../controllers/graphController');

router.get(
  '/:project/:site/:measurement/:start/:end/:interval/:deviceId',
  middleware.validate,
  generalController.graphFunction,
);

router.get('/:hour/:minutes', generalController.calculateHourMinutes);

module.exports = router;
