const express = require('express');

const router = express.Router();

const alertController = require('../controllers/alertController');

router.get('/', alertController.getAllAlerts);
router.post('/alertEndpoint', alertController.alertEndpoint);

module.exports = router;
