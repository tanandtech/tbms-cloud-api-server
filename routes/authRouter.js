const express = require('express');
const middleware = require('../middlewares/userMiddleware');
const authController = require('../controllers/authController');

const router = express.Router();

router.get('/verify', middleware.validate, authController.verify);

module.exports = router;
