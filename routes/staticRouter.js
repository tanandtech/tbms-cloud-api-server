const express = require('express');
const middleware = require('../middlewares/userMiddleware');

const router = express.Router();

const controller = require('../controllers/staticController');

router.get('/:project/sites', middleware.validate, controller.getSites);

router.get('/:project/:site/data', middleware.validate, controller.getSiteData);

module.exports = router;
