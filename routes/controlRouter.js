const express = require('express');
const middleware = require('../middlewares/userMiddleware');

const router = express.Router();

const controller = require('../controllers/controlController');

router.post('/', middleware.validate, controller.command);

module.exports = router;
