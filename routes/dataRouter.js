const express = require('express');

const router = express.Router();

const middleware = require('../middlewares/userMiddleware');
const dataController = require('../controllers/dataController');

router.get(
  '/:project/:site/:measurement/:deviceId',
  middleware.validate,
  dataController.latestFunction,
);

module.exports = router;
