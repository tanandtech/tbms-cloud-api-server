exports.successResponse = (data) => ({
  status: 'OK',
  message: 'OK',
  data,
});

exports.failedResponse = (msg) => ({
  status: 'NOK',
  message: msg,
  data: 'NOK',
});
