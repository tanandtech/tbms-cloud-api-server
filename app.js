const express = require('express');
const cors = require('cors');
const { info } = require('tanand-logger')('API');
const config = require('config');
const { failedResponse } = require('./helpers/constructResponse');

const webConfig = config.web;

const app = express();

const logErrors = (err, req, res, next) => {
  if (err) {
    console.error(err.stack);
  }
  next(err);
};

const errorHandler = (err, req, res, next) => {
  if (!err) {
    next();
    return;
  }

  res.status(500).send(failedResponse(err.message));
};

// API connection
app.use(express.json());
app.use(cors());

app.use('/user', require('./routes/userRouter'));
app.use('/graph', require('./routes/graphRouter'));
app.use('/data', require('./routes/dataRouter'));
app.use('/alert', require('./routes/alertRouter'));
app.use('/control', require('./routes/controlRouter'));
app.use('/benchmark', require('./routes/benchmarkRouter'));
app.use('/static', require('./routes/staticRouter'));
app.use('/auth', require('./routes/authRouter'));

app.use(logErrors);
app.use(errorHandler);

const port = webConfig.port || 3006;
app.listen(port);

info(`API Server Started at Port ${port}`);
