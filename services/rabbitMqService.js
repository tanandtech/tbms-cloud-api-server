const amqp = require('amqplib/callback_api');
const config = require('config');
const { info, error } = require('tanand-logger')('PRODUCER');

const { rabbitmq: rabbitMqSettings } = config;

let connection;
let channel;

const closeOnError = (e) => {
  if (!e) return false;
  error('Error', e);
  connection.close();
  return true;
};

const start = () => {
  connection.createConfirmChannel((err, chan) => {
    if (closeOnError(err)) return;

    chan.on('error', (e) => {
      error('Channel Error', e.message);
    });

    chan.on('close', () => {
      info('AMQP channel closed');
    });

    channel = chan;
  });
};

const initialize = () => {
  amqp.connect(rabbitMqSettings, (err, conn) => {
    if (err) {
      error(err.message);
      return setTimeout(initialize, 1000);
    }

    conn.on('error', (err) => {
      if (err.message !== 'Connection closing') error('Connection Error', err.message);
    });

    conn.on('close', () => {
      error('Reconnecting');
      return setTimeout(initialize, 1000);
    });

    info('connected');
    connection = conn;
    start();
  });
};

const send = (msg) =>
  new Promise((resolve, reject) => {
    try {
      if (!channel) {
        info('Channel Not Yet Ready');
        return reject('Channel Not Yet Ready');
      }

      channel.assertQueue(msg.topic, {
        durable: true,
      });

      channel.sendToQueue(msg.topic, Buffer.from(JSON.stringify(msg)), {}, (e, _) => {
        if (e) {
          channel.close();
          connection.close();
          return reject(`Error sending message ${e}\nFailed message: ${msg}`);
        }
        return resolve('Message sent');
      });
    } catch (e) {
      error('Error Publishing Message', e.message);
      return reject(e.message);
    }
  });

initialize();

module.exports = {
  send,
};
