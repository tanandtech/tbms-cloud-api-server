global.fetch = require('node-fetch');
const config = require('config');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');

const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const request = require('request');

// Configs
const { aws } = config;

global.navigator = () => null;

const poolData = {
  UserPoolId: aws.userPoodId,
  ClientId: aws.clientId,
};

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

exports.Register = function (body, callback) {
  const { name, email, password } = body;
  const attributeList = [];

  attributeList.push(
    new AmazonCognitoIdentity.CognitoUserAttribute({ Name: 'email', Value: email }),
  );
  userPool.signUp(name, password, attributeList, null, (err, result) => {
    if (err) callback(err);
    const cognitoUser = result.user;
    callback(null, cognitoUser);
  });
};

exports.Login = function (body, callback) {
  const { name: userName, password } = body;
  const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
    Username: userName,
    Password: password,
  });
  const userData = {
    Username: userName,
    Pool: userPool,
  };
  const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
  cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess(result) {
      const accesstoken = result.getAccessToken().getJwtToken();
      const idToken = result.getIdToken().getJwtToken();
      callback(null, result);
    },
    onFailure(err) {
      callback(err);
    },
  });
};

exports.validate = (token) =>
  new Promise((resolve, reject) => {
    request(
      {
        url: `https://cognito-idp.${aws.region}.amazonaws.com/${aws.userPoodId}/.well-known/jwks.json`,
        json: true,
      },
      (error, response, body) => {
        if (!error && response.statusCode === 200) {
          const pems = {};

          body.keys.forEach((k) => {
            pems[k.kid] = jwkToPem({ kty: k.kty, n: k.n, e: k.e });
          });

          const decodedJwt = jwt.decode(token, { complete: true });
          if (!decodedJwt) {
            console.log('Not a valid JWT token');
            return reject(new Error('invalidToken'));
          }

          const pem = pems[decodedJwt.header.kid];
          if (!pem) {
            console.log('Invalid token');
            return reject(new Error('invalidToken'));
          }

          return jwt.verify(token, pem, (err, payload) => {
            if (err) {
              console.log('Invalid Token.');
              return reject(new Error('invalidToken'));
            }
            console.log('Valid Token.');
            return resolve(payload);
          });
        }
        console.log('Error! Unable to download JWKs');
        return reject(new Error('verifyTokenFailed'));
      },
    );
  });
