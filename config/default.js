const config = {};

config.web = {
  port: 3006,
};

config.influx = {
  url: 'http://18.138.186.229:8086',
  token: 'YOURTOKEN',
  org: 'tanand-technology',
};

config.postgres = {
  user: 'postgres',
  host: 'localhost',
  database: 'alerts',
  password: 'postgres',
  port: 5432,
};

config.aws = {
  userPoodId: 'ap-southeast-1_6ywtutj7j',
  clientId: '6mpv6vdo7kkhcen06fmfq4s8b6',
  region: 'ap-southeast-1',
};

config.rabbitmq = {
  protocol: 'amqp',
  hostname: 'localhost',
  port: 5672,
  username: 'user',
  password: 'password',
  vhost: '/',
  queueTopic: 'control',
  authMechanism: ['PLAIN', 'AMQPLAIN', 'EXTERNAL'],
};

module.exports = config;
