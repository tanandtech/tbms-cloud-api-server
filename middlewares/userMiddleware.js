// Imports
// const request = require('request');
// const jwkToPem = require('jwk-to-pem');
// const jwt = require('jsonwebtoken');
// const config = require('config');

// // Configs
// const { aws } = config;
const awsAuthService = require('../services/awsAuthService');

// to validate token and also set user payload into response object
exports.validate = async (req, res, next) => {
  const token = req.headers.authorization;
  try {
    const payload = await awsAuthService.validate(token);
    res.locals.user = payload;
    next();
  } catch (err) {
    if (err.message === 'invalidToken') {
      res.status(401).send('Invalid token');
    } else {
      res.status(500).send(err.message);
    }
  }
};

// exports.validate = (req, res, next) => {
//   const token = req.headers.authorization;
//   request(
//     {
//       url: `https://cognito-idp.${aws.region}.amazonaws.com/${aws.userPoodId}/.well-known/jwks.json`,
//       json: true,
//     },
//     (error, response, body) => {
//       if (!error && response.statusCode === 200) {
//         const pems = {};

//         body.keys.forEach((k) => {
//           pems[k.kid] = jwkToPem({ kty: k.kty, n: k.n, e: k.e });
//         });

//         const decodedJwt = jwt.decode(token, { complete: true });
//         if (!decodedJwt) {
//           console.log('Not a valid JWT token');
//           return res.status(401).send('Invalid token');
//         }

//         const pem = pems[decodedJwt.header.kid];
//         if (!pem) {
//           console.log('Invalid token');
//           return res.status(401).send('Invalid token');
//         }

//         return jwt.verify(token, pem, (err, _payload) => {
//           if (err) {
//             console.log('Invalid Token.');
//             return res.status(401).send('Invalid token');
//           }
//           console.log('Valid Token.');
//           return next();
//         });
//       }
//       console.log('Error! Unable to download JWKs');
//       return res.status(500).send('Error! Unable to download JWKs');
//     },
//   );
// };
