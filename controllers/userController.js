/* eslint-disable no-param-reassign */
const awsAuthService = require('../services/awsAuthService');
const { successResponse, failedResponse } = require('../helpers/constructResponse');

exports.login = (req, res) => {
  awsAuthService.Login(req.body, (err, result) => {
    if (err) {
      res.status(401).send(failedResponse(result));
    } else {
      res.status(200).send(successResponse(result));
    }
  });
};
