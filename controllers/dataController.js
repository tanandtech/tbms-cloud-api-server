/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
const _ = require('lodash');
const moment = require('moment');

const { successResponse, failedResponse } = require('../helpers/constructResponse');
const influxModule = require('../database/influxService');

exports.latestFunction = async (req, res) => {
  const { project, site, measurement, deviceId } = req.params;
  const { fields } = req.query;
  try {
    const rawData = await influxModule.getLatestData(project, site, measurement, deviceId, fields);

    const result = rawData.map((r) => ({
      field: r._field,
      timestamp: moment(r._time).unix(),
      deviceId: r.deviceId,
      value: r._value,
    }));
    // const groupByTable = _.groupBy(rawData, 'table');

    // const result = [];
    // for (const array of Object.values(groupByTable)) {
    //   const r = array
    //     .filter((d) => d._value != null)
    //     .map((d) => ({
    //       field: d._field,
    //       value: d._value,
    //       timestamp: moment(d._time).unix(),
    //       deviceId: d.deviceId,
    //     }));
    //   result.push(...r);
    // }
    // console.log(result);
    res.status(200).send(successResponse(result));
  } catch (e) {
    console.error(e.message);
    res.status(500).send(failedResponse(e.message));
  }
};
