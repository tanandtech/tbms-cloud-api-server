/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
const _ = require('lodash');
const moment = require('moment');

const { successResponse, failedResponse } = require('../helpers/constructResponse');
const influxModule = require('../database/influxService');
const pgSvc = require('../database/postgresService');

const intervalKeys = {
  '5m': 'group_5m_day',
  300: 'group_5m_day',
  '10m': 'group_10m_day',
  600: 'group_10m_day',
  '30m': 'group_30m_day',
  1800: 'group_30m_day',
  '1h': 'group_hour_day',
  3600: 'group_hour_day',
  dom: 'group_dom',
  dow: 'group_dow',
};

const benchmarkFields = [
  'energyUsage',
  'current1',
  'current2',
  'current3',
  'totalPwr',
  'temperature',
  'humidity',
  'supplyTemp',
  'returnTemp',
  'supplyHumid',
  'returnHumid',
  'inHumid',
  'inTemp',
  'outHumid',
  'outTemp',
  'btu',
  'chws',
  'chwr',
  'cws',
  'cwr',
  'valve',
];

const benchmarkAgg = {
  energyUsage: 'sum',
};

function round(date, duration, method) {
  return moment(Math[method](+date / +duration) * +duration);
}

exports.getBenchmark = async (req, res) => {
  const { project, site, deviceId } = req.params;
  try {
    const benchmarkResult = await pgSvc.getBenchmark(project, site, deviceId);
    return res.send(
      successResponse({
        project,
        site,
        deviceId,
        start: benchmarkResult.start_timestamp,
        end: benchmarkResult.end_timestamp,
        createdTimestamp: benchmarkResult.created_timestamp,
        keys: benchmarkResult.keys,
      }),
    );
  } catch (e) {
    console.error('failed', e.message);
    return res.status(500).send(failedResponse('failed'));
  }
};

exports.setBenchmark = async (req, res) => {
  const { project, site, deviceId } = req.params;
  const { start, end } = req.body;
  if (!deviceId || !start || !end) {
    return res.status(400).send(failedResponse('incompleteParameters'));
  }

  try {
    console.log('setBenchmark', project, site, deviceId, start, end);
    // select available field keys from influxdb
    // set benchmark for each of the keys
    const fieldsData = await influxModule.getFieldKeys(project, site, deviceId);
    // eslint-disable-next-line no-underscore-dangle
    const availableFields = fieldsData.map((d) => d._value);
    const fields = _.intersection(availableFields, benchmarkFields);

    // check if there is existing benchmark and drop all data if there are
    const benchmarkResult = await pgSvc.getBenchmark(project, site, deviceId);
    if (benchmarkResult) {
      pgSvc.deleteBenchmarkData(benchmarkResult.id);
    }

    const startTime = moment.unix(parseInt(start, 10));
    const endTime = moment.unix(parseInt(end, 10));

    // 1 - create / update benchmark record
    const result = await pgSvc.createBenchmark(
      project,
      site,
      deviceId,
      startTime.unix(),
      endTime.unix(),
      fields,
    );

    console.log(`Record create @ id ${result.id}`);

    const benchmarkId = result.id;

    // 2 - generate benchmark data for each field
    const promises = fields.map(async (field) => {
      const data = await influxModule.getGraphData(
        project,
        site,
        deviceId,
        field,
        start,
        end,
        300,
        benchmarkAgg[field] || 'mean',
      );

      const points = data.map((d) => {
        const timestamp = moment(d._time).subtract(300, 'seconds');
        if (timestamp.unix() === 1612755000) {
          console.log('Field', field, d);
        }
        const timestamp10m = round(timestamp, moment.duration(10, 'minute'), 'floor');
        const timestamp30m = round(timestamp, moment.duration(30, 'minute'), 'floor');
        const timestamp1h = moment(timestamp).startOf('hour');
        const timestamp1d = moment(timestamp).startOf('day');
        const dayOfMonth = moment(timestamp).date();
        const dayOfWeek = moment(timestamp).isoWeekday(); // 1 = Monday, 7 = Sunday
        const group5m = timestamp.diff(timestamp1d, 'seconds');
        const group10m = timestamp10m.diff(timestamp1d, 'seconds');
        const group30m = timestamp30m.diff(timestamp1d, 'seconds');
        const group1h = timestamp1h.diff(timestamp1d, 'seconds');

        return {
          benchmarkId,
          key: field,
          timestamp: timestamp.unix(),
          value: d._value,
          timestamp_10m: timestamp10m.unix(),
          timestamp_30m: timestamp30m.unix(),
          timestamp_1h: timestamp1h.unix(),
          timestamp_1d: timestamp1d.unix(),
          group_dow: dayOfWeek,
          group_dom: dayOfMonth,
          group_5m: group5m,
          group_10m: group10m,
          group_30m: group30m,
          group_1h: group1h,
        };
      });

      return pgSvc.createBenchmarkData(points);
    });
    await Promise.all(promises);
    return res.send(successResponse());
  } catch (e) {
    console.error('failed', e);
    return res.status(500).send(failedResponse('failed'));
  }
};

// to return average value of benchmark
// exports.getBenchmark = async (req, res) => {
//   const { project, site, deviceId } = req.params;
//   const { keys } = req.query;

//   if (!keys) {
//     return res.status(400).send(failedResponse('missingKeys'));
//   }

//   const benchmark = await pgSvc.getBenchmark(project, site, deviceId);

//   if (!benchmark) {
//     return res.status(400).send(failedResponse('noBenchmarkSet'));
//   }

//   const result = await pgSvc.getBenchmarkData(benchmark.id, keys.split(','));
//   return res.send(result.rows);
// };

// /:project/:site/:deviceId/:agg
exports.getBenchmarkAgg = async (req, res) => {
  const { project, site, deviceId, agg } = req.params;
  const { keys, dow, groupInterval, groupAgg } = req.query;

  if (!keys) {
    return res.status(400).send(failedResponse('missingKeys'));
  }

  const groupIntervalKey = intervalKeys[groupInterval];
  if (!groupIntervalKey) {
    return res.status(400).send(failedResponse('unsupportedInterval'));
  }

  try {
    const benchmark = await pgSvc.getBenchmark(project, site, deviceId);

    if (!benchmark) {
      return res.status(400).send(failedResponse('noBenchmarkSet'));
    }

    const data = await pgSvc.getBenchmarkAgg(
      benchmark.id,
      agg,
      dow,
      groupIntervalKey,
      keys.split(','),
      groupAgg,
    );

    const result = data.rows.map((row) => ({
      key: row.key,
      value: row.value,
    }));
    return res.send(successResponse(result));
  } catch (e) {
    console.log(e);
    return res.status(500).send(failedResponse('Something went wrong'));
  }
};

// /:project/:site/:deviceId/:interval/:agg
exports.getBenchmarkAggData = async (req, res) => {
  const { project, site, deviceId, interval, agg } = req.params;
  const { keys, dow, timestampBase } = req.query;

  if (!keys) {
    return res.status(400).send(failedResponse('missingKeys'));
  }

  const intervalKey = intervalKeys[interval];
  if (!intervalKey) {
    return res.status(400).send(failedResponse('unsupportedInterval'));
  }

  try {
    const benchmark = await pgSvc.getBenchmark(project, site, deviceId);

    if (!benchmark) {
      return res.status(400).send(failedResponse('noBenchmarkSet'));
    }

    const data = await pgSvc.getBenchmarkAggData(
      benchmark.id,
      agg,
      dow,
      intervalKey,
      keys.split(','),
    );

    const base = timestampBase ? parseInt(timestampBase, 10) : 0;
    const result = data.rows.map((row) => ({
      key: row.key,
      timestamp: base + row.timestamp,
      value: row.value,
    }));
    return res.send(successResponse(result));
  } catch (e) {
    console.log(e);
    return res.status(500).send(failedResponse('Something went wrong'));
  }
};
