/* eslint-disable no-param-reassign */
const _ = require('lodash');
const pgSvc = require('../database/postgresService');
const { successResponse } = require('../helpers/constructResponse');

exports.getSiteData = async (req, res, next) => {
  const { project, site } = req.params;
  try {
    const siteRes = await pgSvc.getSite(site);
    if (siteRes.rowCount === 1) {
      // get site data
      const siteData = siteRes.rows[0];
      const result = siteData;

      // get areas
      const areaRes = await pgSvc.getAreas(site);
      // result.areas = areaRes.rows.map((r) => _.omit(r, 'site_id'));
      const areasPromise = areaRes.rows.map(async (ar) => {
        const eqRes = await pgSvc.getEquipmentsByArea(ar.id);
        const area = _.omit(ar, 'site_id');
        area.equipments = eqRes.rows.map((r) => r.id);
        return area;
      });

      result.areas = await Promise.all(areasPromise);

      const equipmentRes = await pgSvc.getEquipments(site);
      const equipmentsPromise = equipmentRes.rows.map(async (eq) => {
        const eaRes = await pgSvc.getAreaByEquipment(eq.id);
        const equipment = _.omit(eq, 'site_id');
        equipment.areas = eaRes.rows.map((r) => r.id);
        return equipment;
      });

      result.equipments = await Promise.all(equipmentsPromise);

      res.send(successResponse(result));
    } else {
      throw Error('SiteNotFound');
    }
  } catch (err) {
    next(err);
  }
};

exports.getSites = async (req, res, next) => {
  const { project } = req.params;
  pgSvc
    .getSites(project)
    .then((data) => {
      res.send(successResponse(data.rows));
    })
    .catch(next);
};
