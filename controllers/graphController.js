/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
const _ = require('lodash');
const moment = require('moment');

const { successResponse, failedResponse } = require('../helpers/constructResponse');
const influxModule = require('../database/influxService');

exports.graphFunction = (req, res) => {
  const { project, site, measurement, start, end, interval, deviceId } = req.params;
  const startTs = parseInt(start, 10);
  const endTs = parseInt(end, 10);
  if (endTs < startTs)
    return res.status(400).send(failedResponse('end cannot be earlier than start'));

  influxModule
    .flux(project, site, measurement, startTs, endTs, interval, deviceId)
    .then((data) => {
      const deviceGrouping = _.mapValues(_.groupBy(data, 'table'), (device) =>
        device.map((data) => _.omit(data, 'table')),
      );

      // unnest nested array with flatten
      const result = _.flatten(
        Object.values(deviceGrouping).map((array) =>
          array
            .filter((val) => val._value != null)
            .map((val) => ({
              field: val._field,
              value: val._value,
              timestamp: moment(val._time).unix(),
              deviceId: val.deviceId,
              project: val.project,
              site: val.site,
            })),
        ),
      );

      // HK 20210205 - Changed to code above due to eslint error
      // const result = [];
      // for (const [table, array] of Object.entries(deviceGrouping)) {
      //   array.forEach((data) => {
      //     if (data._value) {
      //       const res = {
      //         field: data._field,
      //         value: data._value,
      //         timestamp: moment(data._time).unix(),
      //         deviceId: data.deviceId,
      //         project: data.project,
      //         site: data.site,
      //       };

      //       result.push(res);
      //     }
      //   });
      // }

      res.status(200).send(successResponse(result));
    })
    .catch((e) => {
      console.log('reach', e);
      res.status(400).send(failedResponse(e));
    });
};

exports.calculateHourMinutes = (req, res) => {
  const { hour, minutes } = req.params;
  const totalMinutes = parseFloat(hour) * 60 + parseFloat(minutes);
  const totalHour = Math.floor(totalMinutes / 60);
  const remainingMinutes = totalMinutes % 60;
  const payload = {
    totalHour,
    remainingMinutes,
  };
  res.status(200).send(successResponse(payload));
};
