const response = require('../helpers/constructResponse');
const { send } = require('../services/rabbitMqService');

exports.command = (req, res) => {
  send(req.body)
    .then(() => res.status(200).send(response.successResponse('OK')))
    .catch((e) => res.status(400).send(response.failedResponse(e)));
};
