/* eslint-disable no-param-reassign */
const { info } = require('tanand-logger')('API');
const { successResponse, failedResponse } = require('../helpers/constructResponse');
const postgres = require('../database/postgresService');

exports.getAllAlerts = (req, res) => {
  const { project, site } = req.body;
  postgres
    .getAllAlerts(project, site)
    .then((data) => {
      res.status(200).send(successResponse(data.rows));
    })
    .catch((e) => {
      res.status(400).send(failedResponse(e));
    });
};

exports.alertEndpoint = (req, res) => {
  const {
    project,
    site,
    deviceId,
    _level,
    _type,
    _status_timestamp: statusTimestamp,
    _check_name: checkName,
  } = req.body;
  const val = _type === 'threshold' ? req.body[checkName.substr(0, checkName.indexOf('/'))] : null;
  postgres
    .insertAlert(project, site, deviceId, _level, _type, val, statusTimestamp)
    .then(() => {
      res.status(200).send(successResponse('OK'));
    })
    .catch((e) => {
      res.status(400).send(failedResponse(e));
    });
};
